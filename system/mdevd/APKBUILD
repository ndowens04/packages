# Contributor: Laurent Bercot <ska-adelie@skarnet.org>
# Maintainer: Laurent Bercot <ska-adelie@skarnet.org>
pkgname=mdevd
pkgver=0.1.1.2
pkgrel=0
pkgdesc="A small uevent manager daemon"
url="https://skarnet.org/software/$pkgname/"
arch="all"
options="!check"  # No test suite.
license="ISC"
_skalibs_version=2.9.1.0
makedepends="skalibs-dev>=$_skalibs_version"
subpackages="$pkgname-doc $pkgname-openrc"
source="https://skarnet.org/software/$pkgname/$pkgname-$pkgver.tar.gz mdev.conf mdevd.run mdevd.initd"

build() {
	./configure --enable-allstatic --enable-static-libc
	make
}

package() {
	make DESTDIR="$pkgdir" install
	mkdir -p -m 0755 "$pkgdir/etc"
	cp -f "$srcdir/mdev.conf" "$pkgdir/etc/"
	chmod 0644 "$pkgdir/etc/mdev.conf"
}


doc() {
        pkgdesc="$pkgdesc (documentation)"
        depends=
        install_if="docs $pkgname=$pkgver-r$pkgrel"
        mkdir -p "$subpkgdir/usr/share/doc"
        cp -a "$builddir/doc" "$subpkgdir/usr/share/doc/$pkgname"
}


openrc() {
	rldir="$subpkgdir"/etc/runlevels/sysinit
	svcdir="$subpkgdir/etc/s6-linux-init/current/run-image/service/mdevd"
        default_openrc
        mkdir -p "$rldir" "$svcdir"
	cp -f "$srcdir/mdevd.run" "$svcdir/run"
	chmod 0755 "$svcdir/run"
	echo 3 > "$svcdir/notification-fd"
	touch "$svcdir/down"
        ln -s ../../init.d/mdevd "$rldir/mdevd"
}

sha512sums="7d7a25facde1291854f564e805cc2bf7e7e0b06aaa4b631a640cf999007a3ef063ec68e8973559ec69023f40bb6ed24937d4d8510fd697f34a97345a22ede5b5  mdevd-0.1.1.2.tar.gz
b237443837c3d76f8964e2b1a48c2278af7b4a4e7f735da6fc861b7598cbf50017e961e5a4fd1f20dd9cb7dd46f4cdf68144539d909b1f245e110b75c2d849db  mdev.conf
31231b28d0b980dda634e8b043a2ee31960493d60c2c9aac8a4f3f68ca1d130342569af2acd0bc04d9d8008b8e234ba949b64d9ec3ff1bded64b4e4f0ce3786b  mdevd.run
f6f9eebf49c2de6abde9cfb7a9d7b3a6b3afdd3e01ba4155f1b95dfa27e522363059b69cec19305881773d776ffeccf6c1106d537d01780468bd3b9846edb2cc  mdevd.initd"
