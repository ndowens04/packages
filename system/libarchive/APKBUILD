# Contributor: Sergei Lukin <sergej.lukin@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libarchive
pkgver=3.4.3
pkgrel=0
pkgdesc="Multi-format archive and compression library"
url="https://libarchive.org/"
arch="all"
options="!check"  # needs EUC-JP and KOI8R support in iconv
license="BSD-2-Clause AND BSD-3-Clause AND Public-Domain"
depends=""
makedepends="zlib-dev bzip2-dev xz-dev lz4-dev acl-dev openssl-dev expat-dev
	attr-dev zstd-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-tools"
source="https://github.com/libarchive/libarchive/releases/download/v$pkgver/$pkgname-$pkgver.tar.gz
	seek-error.patch
	"

# secfixes:
#   3.4.2-r0:
#     - CVE-2020-9308
#   3.3.2-r1:
#     - CVE-2017-14166

build () {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--with-expat \
		--without-xml2 \
		--with-bz2lib \
		--with-zlib \
		--with-lzma \
		--with-lz4 \
		--enable-acl \
		--enable-xattr \
		ac_cv_header_linux_fiemap_h=no
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

tools() {
	pkgdesc="libarchive tools - bsdtar and bsdcpio"

	mkdir -p "$subpkgdir"/usr/
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
	ln -s bsdtar "$subpkgdir"/usr/bin/tar
	ln -s bsdcpio "$subpkgdir"/usr/bin/cpio
}

sha512sums="d00167dec6e65a0b17b46a1e3bb0242d85716dbc637afd233360cb515b2750dafe0ff0644b9e01ad23534340b405a8551f496c5e39fba9ee99355a515580d65d  libarchive-3.4.3.tar.gz
ff2567f243ba7e9ce20bc4f7fa422a922c5c23049004efdd8f71f29f93ab9be9aadd4c100e8c6dca318442d583fbad9bd6466017a23f83af18b9808c718b9fce  seek-error.patch"
