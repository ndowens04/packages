# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=xz
pkgver=5.2.5
pkgrel=0
pkgdesc="Library and command line tools for XZ and LZMA compressed files"
url="https://tukaani.org/xz/"
arch="all"
license="Public-Domain AND LGPL-2.1+"
depends=""
makedepends=""
subpackages="$pkgname-doc $pkgname-dev $pkgname-lang $pkgname-libs"
source="https://tukaani.org/xz/xz-$pkgver.tar.gz
	dont-use-libdir-for-pkgconfig.patch
	"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--libdir=/lib \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--disable-rpath \
		--disable-werror

	sed 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' \
		-i libtool
	sed 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' \
		-i libtool

	make
}

check() {
	make check
}

package() {
	make -C "$builddir" DESTDIR="$pkgdir" install
	install -Dm644 "$builddir"/COPYING \
		"$pkgdir"/usr/share/licenses/$pkgname
}

sha512sums="7443674247deda2935220fbc4dfc7665e5bb5a260be8ad858c8bd7d7b9f0f868f04ea45e62eb17c0a5e6a2de7c7500ad2d201e2d668c48ca29bd9eea5a73a3ce  xz-5.2.5.tar.gz
9310ae2568dd6ac474e3cb9895e1339ca2dbe8834f856edbb7d2264c0019bde4bbd94aa1edd34e5c8d0aed1f35a1877b0e053ed08a270835ea81e59c7be5edb3  dont-use-libdir-for-pkgconfig.patch"
