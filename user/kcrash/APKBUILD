# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kcrash
pkgver=5.74.0
pkgrel=0
pkgdesc="Framework for gracefully handling software errors~"
url="https://www.kde.org/"
arch="all"
options="!checkroot !check"  # Requires running KDE Plasma 5 session
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev libx11-dev libxext-dev libice-dev kcoreaddons-dev
	kwindowsystem-dev"
checkdepends="xkeyboard-config"
makedepends="$depends_dev cmake extra-cmake-modules doxygen graphviz
	qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kcrash-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="082a18aa69e62b05c262111396fbda62a5a5192f345ed9563f9f549ca05be34d6027e7339aef66d46d02a3fa7abc57c362db49dbbb71e94111815644890f0447  kcrash-5.74.0.tar.xz"
