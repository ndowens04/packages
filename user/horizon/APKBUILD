# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=horizon
pkgver=0.9.5
pkgrel=1
pkgdesc="Installation tools for Adélie Linux"
url="https://horizon.adelielinux.org/"
arch="all"
options="!archcheck !check"  # Unpackaged dependency ruby-aruba.
license="AGPL-3.0+"
depends=""
checkdepends="ruby-aruba ruby-rspec valgrind"
# Second group is the Qt5 UI deps.
makedepends="bcnm-dev boost-dev cmake curl-dev eudev-dev libarchive-dev
	linux-headers parted-dev skalibs-dev util-linux-dev

	libcap-dev libx11-dev libxkbfile-dev qt5-qtbase-dev"
subpackages="$pkgname-image $pkgname-dbg $pkgname-dev $pkgname-doc $pkgname-qt5
	$pkgname-tools $pkgname-wizard $pkgname-boot"
source="https://distfiles.adelielinux.org/source/horizon-$pkgver.tar.xz
	bootloader.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DBUILD_SHARED_LIBS=True \
		-DBUILD_TOOLS=ON \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS -Wno-format-truncation" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DUNSUPPORTED_NONFREE_FIRMWARE=ON \
		-DINSTALL=ON \
		${CMAKE_CROSSOPTS} \
		-Bbuild
	make -C build

	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DBUILD_SHARED_LIBS=False \
		-DBUILD_TOOLS=OFF \
		-DBUILD_UI=ON \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS -Wno-format-truncation" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DUNSUPPORTED_NONFREE_FIRMWARE=ON \
		-DINSTALL=OFF \
		${CMAKE_CROSSOPTS} \
		-Bbuild-wizard
	make -C build-wizard
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

image() {
	pkgdesc="Tools for generating images using HorizonScript"
	depends="dracut mtools"
	mkdir -p "$subpkgdir"/usr/bin
	mkdir -p "$subpkgdir"/usr/lib
	mkdir -p "$subpkgdir"/usr/share/horizon

	mv "$pkgdir"/usr/bin/hscript-image "$subpkgdir"/usr/bin/
	mv "$pkgdir"/usr/lib/libhi-backends.so "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/share/horizon/iso "$subpkgdir"/usr/share/horizon/
}

qt5() {
	pkgdesc="Qt 5 installation wizard for Adélie Linux"
	depends="horizon"
	mkdir -p "$subpkgdir"/etc/horizon
	mkdir -p "$subpkgdir"/usr/bin
	mkdir -p "$subpkgdir"/var/log/horizon

	for _bin in horizon-ppc64-detect horizon-qt5 horizon-run-qt5; do
		mv "$pkgdir"/usr/bin/$_bin "$subpkgdir"/usr/bin/
	done
}

tools() {
	pkgdesc="Tools for authoring HorizonScript files"
	mkdir -p "$subpkgdir"/usr/bin

	for _bin in hscript-fromjson hscript-simulate hscript-validate; do
		mv "$pkgdir"/usr/bin/$_bin "$subpkgdir"/usr/bin/
	done
}

wizard() {
	pkgdesc="Wizard to generate a HorizonScript for another computer"

	make DESTDIR="$subpkgdir" -C "$builddir"/build-wizard install
	rm -r "$subpkgdir"/usr/include
	rm -r "$subpkgdir"/usr/lib
	rm -r "$subpkgdir"/usr/share
}

boot() {
	pkgdesc="Boot to Horizon"
	depends="horizon-qt5 horizon kwin qt5ct sddm
	netsurf partitionmanager xterm"

	mkdir -p "$subpkgdir"/etc/init.d
	mkdir -p "$subpkgdir"/etc/profile.d
	mkdir -p "$subpkgdir"/etc/runlevels/default
	mkdir -p "$subpkgdir"/etc/sddm.conf.d
	mkdir -p "$subpkgdir"/etc/xdg/qt5ct
	mkdir -p "$subpkgdir"/usr/bin
	mkdir -p "$subpkgdir"/usr/share/qt5ct/qss
	mkdir -p "$subpkgdir"/usr/share/xsessions

	cat >"$subpkgdir"/etc/profile.d/horizon-qt5.sh <<PROFILE
#!/bin/sh
export QT_QUICK_BACKEND=software
export QT_QPA_PLATFORMTHEME=qt5ct
PROFILE
	chmod 755 "$subpkgdir"/etc/profile.d/horizon-qt5.sh
	cat >"$subpkgdir"/etc/xdg/kdeglobals <<GLOBALS
[General]
ColorScheme=Steel
           
[Icons]
Theme=Papirus

[WM]
activeBackground=104,0,0
GLOBALS

	install -D -m644 "$builddir"/boot/horizon.sddm.conf \
		"$subpkgdir"/etc/sddm.conf.d/horizon.conf

	install -D -m755 "$builddir"/boot/gfxdetect \
		"$subpkgdir"/etc/init.d/gfxdetect

	ln -s ../../init.d/gfxdetect \
		"$subpkgdir"/etc/runlevels/default/gfxdetect

	install -D -m755 "$builddir"/boot/start-horizon-qt5 \
		"$subpkgdir"/usr/bin/start-horizon-qt5

	install -D -m644 "$builddir"/boot/kwinrc \
		"$subpkgdir"/etc/xdg/kwinrc

	install -D -m644 "$builddir"/boot/qt5ct.conf \
		"$subpkgdir"/etc/xdg/qt5ct/qt5ct.conf
	install -D -m644 "$builddir"/boot/horizon.qss \
		"$subpkgdir"/usr/share/qt5ct/qss/horizon.qss

	install -D -m644 "$builddir"/boot/horizon-session.desktop \
		"$subpkgdir"/usr/share/xsessions/horizon.desktop
}

sha512sums="373cbf87aa468d0ea41abad07f61cdaa6088cfdcba13c0beff245dbd8e270aa42c3e301a22913299d148e829114bd0b5aa746541c4b4b0c7286da35015c54d88  horizon-0.9.5.tar.xz
88e7c6df4dc2c8c7f6befd5f96f82267fefccb98d0c5e6c26c611323e505f7ee964dcfd2128dbe165c064d953e7afef2787ab49a6c4a834ffe8fd50c00eb26ae  bootloader.patch"
