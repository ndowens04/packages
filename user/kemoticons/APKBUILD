# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kemoticons
pkgver=5.74.0
pkgrel=0
pkgdesc="Emoticons to express emotions in KDE"
url="https://www.kde.org/"
arch="all"
options="!check"  # Test requires accelerated X11 environment.
license="LGPL-2.1+ AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends=""
depends_dev="qt5-qtbase-dev karchive-dev kconfig-dev kservice-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qttools-dev doxygen
	graphviz"
subpackages="$pkgname-dev $pkgname-doc"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kemoticons-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="b4f2a232cac9b997d204b8bdc1a217d643091c7bb723208d3f35268997b6eb07dbb9dff1d26c4732c8dd5f96e0c2e37549293532d412f8c175b5498b6d472ea7  kemoticons-5.74.0.tar.xz"
