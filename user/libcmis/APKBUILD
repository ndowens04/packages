# Contributor: Timo Teräs <timo.teras@iki.fi>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=libcmis
pkgver=0.5.2
pkgrel=1
pkgdesc="CMIS protocol client library for C/C++"
url="https://github.com/tdf/libcmis"
arch="all"
license="MPL-1.1 OR GPL-2.0+ OR LGPL-2.0+"
depends=""
depends_dev="boost-dev"
checkdepends="cppunit-dev"
makedepends="$depends_dev curl-dev libxml2-dev"
subpackages="$pkgname-dev cmis-client:client"
source="https://github.com/tdf/$pkgname/releases/download/v$pkgver/$pkgname-$pkgver.tar.gz
	icu.patch
	"

build() {
	# Note: manpages require docbook2x to build
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--without-man \
		--disable-werror
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

client() {
	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr
}

sha512sums="3dac0eed31d1dd3ac4670e501e0677196811f7875e6d1c253c8d874d6903691cbe4f1c27c8468af07bfd7f79b0d6ec4f933b28cb3bb37dcbea18bc2dd8f6e374  libcmis-0.5.2.tar.gz
0904a529eafa8e1cfdb673e4e7f37150b0b88886cdc32096a89153cb5856d16dc5fee52f39ae5bfea86065b20690a6e14e4b9ad11b36b682c062702b797e82cd  icu.patch"
