# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox-kde@adelielinux.org>
pkgname=libkscreen
pkgver=5.18.5
pkgrel=0
pkgdesc="KDE Plasma screen management software"
url="https://www.kde.org/"
arch="all"
options="!check"  # Requires dbus-x11 and both of them running
license="LGPL-2.1+ AND GPL-2.0+ AND (GPL-2.0-only OR GPL-3.0-only)"
depends=""
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qtx11extras-dev
	qt5-qttools-dev kwayland-dev doxygen"
subpackages="kscreen-doctor:doctor $pkgname-dev $pkgname-doc $pkgname-wayland"
source="https://download.kde.org/stable/plasma/$pkgver/libkscreen-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

doctor() {
	pkgdesc="KDE Plasma screen debugging and management tool"
	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

wayland() {
	pkgdesc="$pkgdesc (Wayland support)"
	install_if="$pkgname=$pkgver-r$pkgrel wayland"
	mkdir -p "$subpkgdir"/usr/lib/qt5/plugins/kf5/kscreen
	mv "$pkgdir"/usr/lib/qt5/plugins/kf5/kscreen/KSC_KWayland.so \
		"$subpkgdir"/usr/lib/qt5/plugins/kf5/kscreen/
}

sha512sums="8fc36bfd5aeb94fada51be1895ff2cf1b9fa98332f1795ac5e9d5339a35d87a24ed15c1f44af85205198c584eb411191f72ef8de370d1f97476d75db1458cb00  libkscreen-5.18.5.tar.xz"
