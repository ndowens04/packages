# Contributor: Laurent Bercot <ska-adelie@skarnet.org>
# Maintainer: Laurent Bercot <ska-adelie@skarnet.org>
pkgname=s6-networking
pkgver=2.3.1.2
pkgrel=0
pkgdesc="skarnet.org's UCSPI TCP tools, access control tools, and network time management utilities."
url="https://skarnet.org/software/$pkgname/"
arch="all"
options="!check"  # No test suite.
license="ISC"
_skalibs_version=2.9
depends="execline"
makedepends="skalibs-dev>=$_skalibs_version skalibs-libs-dev>=$_skalibs_version execline-dev s6-dev s6-libs-dev s6-dns-dev s6-dns-libs-dev bearssl-dev"
subpackages="$pkgname-libs $pkgname-dev $pkgname-libs-dev:libsdev $pkgname-doc"
source="https://skarnet.org/software/$pkgname/$pkgname-$pkgver.tar.gz"

build() {
	./configure \
		--enable-shared \
		--enable-static \
		--disable-allstatic \
		--prefix=/usr \
		--libdir=/usr/lib \
		--libexecdir="/usr/lib/$pkgname" \
		--with-dynlib=/lib \
		--enable-ssl=bearssl
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

libs() {
	pkgdesc="$pkgdesc (shared libraries)"
	depends="skalibs-libs>=$_skalibs_version"
	mkdir -p "$subpkgdir/usr/lib"
	mv "$pkgdir"/usr/lib/*.so.* "$subpkgdir/usr/lib/"
}

dev() {
	pkgdesc="$pkgdesc (development files)"
	depends="skalibs-dev>=$_skalibs_version"
	install_if="dev $pkgname=$pkgver-r$pkgrel"
	mkdir -p "$subpkgdir/usr/include" "$subpkgdir/usr/lib"
	mv "$pkgdir/usr/include" "$subpkgdir/usr/"
	mv "$pkgdir"/usr/lib/*.a "$subpkgdir/usr/lib/"
}

libsdev() {
	pkgdesc="$pkgdesc (development files for dynamic linking)"
	depends="$pkgname-dev"
	mkdir -p "$subpkgdir/usr/lib"
	mv "$pkgdir"/usr/lib/*.so "$subpkgdir/usr/lib/"
}

doc() {
	pkgdesc="$pkgdesc (documentation)"
	depends=""
	install_if="docs $pkgname=$pkgver-r$pkgrel"
	mkdir -p "$subpkgdir/usr/share/doc"
	cp -a "$builddir/doc" "$subpkgdir/usr/share/doc/$pkgname"
}

sha512sums="292bba150fcf4567073690411caebb66b407dbac20dfcc07f8a79d55962ef07f1fcfcf25bac21734605bc171e22805812788b0c12ec01e79f320eef782479d1c  s6-networking-2.3.1.2.tar.gz"
