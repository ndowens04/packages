# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=ruby-rake-compiler
_gemname=${pkgname#ruby-}
pkgver=1.1.1
pkgrel=0
pkgdesc="Provide a standard and simplified way to build and package Ruby extensions"
url="https://github.com/rake-compiler/rake-compiler"
arch="noarch"
license="MIT"
depends="ruby ruby-rake"
checkdepends="ruby-rspec"
makedepends=""
source="$pkgname-$pkgver.tar.gz::https://github.com/rake-compiler/$_gemname/archive/v$pkgver.tar.gz
	gemfile-remove-unwanted-files.patch"
builddir="$srcdir/$_gemname-$pkgver"

build() {
	gem build $_gemname.gemspec
}

check() {
	rspec spec
}

package() {
	gemdir="$pkgdir/$(ruby -e 'puts Gem.default_dir')"

	gem install --local \
		--install-dir "$gemdir" \
		--bindir "$pkgdir/usr/bin" \
		--ignore-dependencies \
		--no-document \
		--verbose \
		$_gemname

	# Remove unnecessary files and empty directories.
	cd "$gemdir"
	rm -r cache build_info doc
}

sha512sums="e9cf9752e48eae912657a34d2f86a2b41f7d57620c6e43b1fa6fb8e5715336b2fdf99bf20a4851889e8b9b7b2e834421c13e108c538abdc3cec968952345361d  ruby-rake-compiler-1.1.1.tar.gz
21feda7887b7fe0978f906d407ecd44d7f25e334e7ee8ff27f73e71d494bd10af4174d7373652b29b1a8eaf95f53f2c8581879088f343e7f045fc265eecf7eac  gemfile-remove-unwanted-files.patch"
