# Contributor: Kiyoshi Aman <adelie@aerdan.vulpine.house>
# Maintainer: Kiyoshi Aman <adelie@aerdan.vulpine.house>
pkgname=lximage-qt
pkgver=0.15.0
pkgrel=0
pkgdesc="Image viewer and screenshot tool for LXQt"
url="https://lxqt.github.io/"
arch="all"
options="!check"  # No test suite.
license="GPL-2.0+"
depends=""
makedepends="cmake extra-cmake-modules lxqt-build-tools>=0.7.0 libexif-dev
	libfm-qt-dev>=${pkgver%.*}.0 qt5-qtx11extras-dev qt5-qttools-dev qt5-qtsvg-dev
	kwindowsystem-dev"
source="https://github.com/lxqt/lximage-qt/releases/download/$pkgver/lximage-qt-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} -Bbuild
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="980fc401b31c5d937877ef35211892984def150a6db6072d9752fa78588ea8ca0827beb5880d46775092c42e93358d3e494e045a50756931129402ded4ecab23  lximage-qt-0.15.0.tar.xz"
