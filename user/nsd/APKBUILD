# Contributor: Mira Ressel <aranea@aixah.de>
# Maintainer: Alyx Wolcott <alyx@leuhta.com>
pkgname=nsd
pkgver=4.3.2
pkgrel=0
pkgdesc="An authoritative only name server"
url="https://www.nlnetlabs.nl/projects/nsd/about/"
arch="all"
options="!check" # No test suite
license="BSD-3-Clause"
depends=""
makedepends="libevent-dev openssl-dev"
subpackages="$pkgname-doc $pkgname-openrc"
install="$pkgname.pre-install"
pkgusers="nsd"
pkggroups="nsd"
source="https://nlnetlabs.nl/downloads/$pkgname/$pkgname-$pkgver.tar.gz
	nsd.confd
	nsd.initd"

# secfixes:
#   4.2.2-r1:
#     - CVE-2019-13207

build() {
	# dnstap has yet unpackaged dependencies
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--localstatedir=/var \
		--with-pidfile=/run/nsd.pid \
		--disable-dnstap \
		--enable-bind8-stats \
		--enable-ratelimit \
		--enable-ratelimit-default-is-off \
		--enable-recvmmsg \
		--with-ssl=/usr \
		--with-libevent=/usr
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

openrc() {
	default_openrc
	install -Dm755 "$srcdir/nsd.initd" "$subpkgdir/etc/init.d/nsd"
	install -Dm644 "$srcdir/nsd.confd" "$subpkgdir/etc/conf.d/nsd"
}

sha512sums="0423aeddc7f60e04718b6ee0954fbdc274576b6ef61472324256e5db696f41e47899b645cbc4d6d8cc2a7dd00190b72f3ef0476c4640dbb80d36ec39f1c9f5c8  nsd-4.3.2.tar.gz
f0ef1d3427e92650239d9d91402810c045fc9223e3f42ce86986422bf2039a0bcc02dffdfe1153d54de5c76c8f2bdc3e34fe341c65b41f2d333b02c00b5b0eae  nsd.confd
139e52dec98792173f06d298574db0d0e6966a06af8a0a3069487beb01fd570c09d22322569b54bacdc43232dbfb99a8c497d4417d2bbfee88bcdd9d1b4d22f7  nsd.initd"
