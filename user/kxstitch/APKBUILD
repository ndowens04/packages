# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kxstitch
pkgver=2.2.0
pkgrel=0
pkgdesc="Cross-stitch pattern editor by KDE"
url="https://userbase.kde.org/KXStitch"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtx11extras-dev
	kdoctools-dev kconfig-dev kconfigwidgets-dev kcompletion-dev ki18n-dev
	kio-dev ktextwidgets-dev kwidgetsaddons-dev kxmlgui-dev imagemagick-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/kxstitch/$pkgver/kxstitch-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="bf09130a35bc605db2bbd6e5e129ac494e8f02b920d4a8d6705b5030a9339306b3b302bd988839e1684da3e5e15e91d655dadf916eb4bf2dc9c1322fd435281d  kxstitch-2.2.0.tar.xz"
