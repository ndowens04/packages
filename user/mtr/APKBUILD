# Contributor: John Keith Hohm <john@hohm.net>
# Maintainer:
pkgname=mtr
pkgver=0.93
pkgrel=0
pkgdesc="Full screen ncurses traceroute tool"
url="https://www.bitwizard.nl/mtr/"
arch="all"
options="!check suid"  # Tests need networking.
license="GPL-2.0-only AND LGPL-3.0-only AND BSD-3-Clause"
depends="ncurses"
makedepends="autoconf automake gtk+2.0-dev libcap-dev ncurses-dev"
subpackages="$pkgname-doc $pkgname-gtk"
source="$pkgname-$pkgver.tar.gz::https://github.com/traviscross/mtr/archive/v$pkgver.tar.gz
	handle-program-suffix.patch
	mtr-gtk.desktop
	"

build() {
	msg "Creating build system..."
	./bootstrap.sh

	mkdir -p mtr curses gtk

	msg "Building curses variant..."
	export GIT_DIR="$builddir"
	export LIBS="-ltinfo"
	cd "$builddir"/curses
	../configure --prefix=/usr \
		--without-gtk
	make

	msg "Building GTK+ variant..."
	cd "$builddir"/gtk
	../configure --prefix=/usr \
		--with-gtk \
		--without-ncurses \
		--program-suffix=-gtk
	make
	unset LIBS GIT_DIR
}

package() {
	make DESTDIR="$pkgdir" -C curses install
}

gtk() {
	pkgdesc="Graphical traceroute tool"
	depends=""
	make DESTDIR="$subpkgdir" -C "$builddir"/gtk install
	install -D -m 644 "$srcdir"/mtr-gtk.desktop \
		"$subpkgdir"/usr/share/applications/mtr-gtk.desktop
	install -D -m 644 "$builddir"/img/mtr_icon.xpm \
		"$subpkgdir"/usr/share/pixmaps/mtr_icon.xpm
	mv "$subpkgdir"/usr/share/man/man8/* "$pkgdir"/../mtr-doc/usr/share/man/man8/
	rm -r "$subpkgdir"/usr/share/man
}

sha512sums="865c394d8dfa234c472832dc205ddc9ec3cf0552475f565782fd4f4e878251c03f1473e2d28d6799e59da4fcb33c5355049f1e6f02e8610c6bbf681ffb78f53e  mtr-0.93.tar.gz
60c11b27c4d20a75010cadc31d5c1ac94094aaaee0c2f0ad14246b00b0d5301f581ef55eaefa48692d4449a8e7580e67abbb58ea3183fae8f449f7969242f1a9  handle-program-suffix.patch
ecf7543e0125fad6d3f17c30f29f1fc8a3b1e2e477802fe8464e436c3cdfa30d0630b8543cc3f022c475228e94ac8f92981df4d8fb08fe01d004be3d78d6da77  mtr-gtk.desktop"
