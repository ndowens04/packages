# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=dracut
pkgver=050
pkgrel=2
pkgdesc="Event-driven initramfs infrastructure"
url="https://dracut.wiki.kernel.org/"
arch="all"
options="!check"  # Test suite is for kernel developers only, requires ext3 rootfs
license="GPL-2.0+"
depends="bash gzip libarchive-tools musl-utils xz"
makedepends="fts-dev kmod-dev"
subpackages="$pkgname-doc $pkgname-bash-completion:bashcomp:noarch
	$pkgname-crypt::noarch $pkgname-lvm::noarch"
source="https://www.kernel.org/pub/linux/utils/boot/$pkgname/$pkgname-$pkgver.tar.xz
	fts.patch
	header-fix.patch
	mount-run-without-noexec.patch
	dracut.easy-boot
	"

prepare() {
	default_prepare
	# Breaks separate /usr with OpenRC
	rm -r "$builddir"/modules.d/98usrmount
}

build() {
	./configure \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	LDLIBS="-lfts" make
	make doc
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	for sysdmod in 00systemd 98dracut-systemd \
	    01systemd-initrd 02systemd-networkd; do
		rm -r "$pkgdir"/usr/lib/dracut/modules.d/$sysdmod
	done

	install -Dm755 "$srcdir"/dracut.easy-boot \
		"$pkgdir"/etc/easy-boot.d/20-dracut
}

bashcomp() {
	depends="dracut"
	pkgdesc="Bash completions for $pkgname"
	install_if="$pkgname=$pkgver-r$pkgrel bash-completion"

	mkdir -p "$subpkgdir"/usr/share/
	mv "$pkgdir"/usr/share/bash-completion \
		"$subpkgdir"/usr/share
}

crypt() {
	depends="cryptsetup dracut lvm2"
	pkgdesc="$pkgname - LUKS / disk encryption support (crypt) module"
	mkdir -p "$subpkgdir"
}

lvm() {
	depends="dracut lvm2"
	pkgdesc="$pkgname - LVM2 module"
	mkdir -p "$subpkgdir"
}

sha512sums="eba046cf1c8013369a398e585e0bff233daa8595d469ce9acc8bbc6a32d55c6a5429d4219db19abbf6001104be05b357f0961f9e66b7f926039a5d3ee7c2b850  dracut-050.tar.xz
ce84e527e441e18494ea9011b4b10cf723ce5981c4717140f43861b6ed3e0f0aa78545be41c111d3338422f2ad158edc8704376c3dca207ae4445c58f54a4574  fts.patch
988f03a3fd2e7ee62409d3c57e8029403513dcec5efb37e64633d989728e4c7b619ce5b8775a04c5a0b654f7f093777d94fe6e4098a99a690c353a94f537e24c  header-fix.patch
d7aa2b35def975ec2a9620d3e8c94da5fad5be51e81ac913b9f3497b3ca62beefb9d4cf8e4ba3b292f89b936373486d0e3184f65eb1eaed972f38d17424a32b1  mount-run-without-noexec.patch
ea576d30e51b2f1c676e29fb81b064b3b8c696a3692dbcf6a31f8a589ab2a426f421bc1002295eb79843cba22d6ddeb04fadb3f99d9e5c00d2d5f53fd3a6c484  dracut.easy-boot"
