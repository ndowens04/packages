# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libkdepim
pkgver=20.08.1
pkgrel=0
pkgdesc="KDE PIM runtime library"
url="https://kontact.kde.org/"
arch="all"
license="LGPL-2.1+"
depends=""
makedepends="qt5-qtbase-dev qt5-qttools-dev cmake extra-cmake-modules boost-dev
	kauth-dev kcalendarcore-dev kcmutils-dev kcodecs-dev kcompletion-dev
	kconfigwidgets-dev kcontacts-dev kcoreaddons-dev ki18n-dev kio-dev
	kitemmodels-dev kitemviews-dev kjobwidgets-dev kldap-dev kmime-dev
	kservice-dev kwallet-dev kwidgetsaddons-dev kwindowsystem-dev
	kxmlgui-dev solid-dev

	akonadi-dev akonadi-contacts-dev akonadi-mime-dev akonadi-search-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkdepim-$pkgver.tar.xz
	lts.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	QT_QPA_PLATFORM=offscreen CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="064e0e9432709dbbcb30e4ff5266d0ce7c960bb35b66cbb1fe4b3feee089ccecb47422cb5258b4d39913d15a02260c4454e174e63c3ce3d2efd37a425e41017c  libkdepim-20.08.1.tar.xz
56e0b6797219252f52c0e3d51015f8abf89b924d1f454911cba3c7de11920811e6a9d18ea52ffbe8fbec82e967c11cbd386b4060b90363b64ec44d603e847cf5  lts.patch"
