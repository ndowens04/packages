# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=solid
pkgver=5.74.0
pkgrel=0
pkgdesc="Platform-independent hardware discovery and access"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends="udisks2 upower"
depends_dev="qt5-qtbase-dev qt5-qtdeclarative-dev eudev-dev"
makedepends="$depends_dev cmake extra-cmake-modules flex bison qt5-qttools-dev
	doxygen"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/solid-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="cc0190327552c63bca8627b96d9f59ec9fb4e4c1842d36c42455e8a458f97f972274765a94b70f311a4105d33f0abbd921b10a2db2b1e7f4c492e92268ca68b1  solid-5.74.0.tar.xz"
