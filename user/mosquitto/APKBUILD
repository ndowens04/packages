# Contributor: Pedro Filipe <xpecex@outlook.com>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=mosquitto
pkgver=1.6.10
pkgrel=0
pkgdesc="An Open Source MQTT Broker"
url="https://mosquitto.org/"
arch="all"
license="EPL-1.0 AND EDL-1.0 AND MIT AND BSD-3-Clause"
depends=""
checkdepends="python3"
makedepends="c-ares-dev openssl-dev util-linux-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-libs $pkgname-openrc
	$pkgname-clients"
install="$pkgname.pre-install"
source="http://mosquitto.org/files/source/$pkgname-$pkgver.tar.gz
	mosquitto.initd
	disable-flaky-test.patch
	openrc-conf.patch
	"

# secfixes:
#   1.6.6-r0:
#     - CVE-2019-11779
#   1.5.6-r0:
#     - CVE-2018-12546
#     - CVE-2018-12550
#     - CVE-2018-12551
#   1.5.3-r0:
#     - CVE-2018-12543
#   1.4.15-r0:
#     - CVE-2017-7652
#     - CVE-2017-7651
#   1.4.13-r0:
#     - CVE-2017-9868
#   1.4.12-r0:
#     - CVE-2017-7650

build() {
	make \
		WITH_ADNS=no \
		WITH_MEMORY_TRACKING=no \
		WITH_SRV=yes \
		prefix=/usr
}

check() {
	# Unit tests require cunit, so skip them.
	make -j1 -C test/broker test
	make -j1 -C test/lib test
}

package() {
	make prefix=/usr DESTDIR="$pkgdir" install

	# C++ bindings are deprecated, so don't install them.
	make -C lib/cpp prefix=/usr DESTDIR="$pkgdir" uninstall
	rm "$pkgdir"/usr/lib/pkgconfig/libmosquittopp.pc

	mv "$pkgdir"/usr/sbin/mosquitto "$pkgdir"/usr/bin
	mv "$pkgdir"/etc/mosquitto/mosquitto.conf.example \
		"$pkgdir"/etc/mosquitto/mosquitto.conf

	install -Dm755 "$srcdir"/mosquitto.initd \
		"$pkgdir"/etc/init.d/mosquitto
}

clients() {
	pkgdesc="Mosquitto command line MQTT clients"

	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/mosquitto_[ps]ub "$subpkgdir"/usr/bin/
}

sha512sums="2050a7a7af864057a177728f1cab9f6db70d6d1ea609d90c637723c2c9cfc9716429739302abd6742bde932716fae340854fff35956693dfee4ef31ce7b04a25  mosquitto-1.6.10.tar.gz
681fddc737b3ef3e6c052f0c854a730df290352640a18a63e23ef83d14c425558aff87805d1eb95e44de101b5df48872173af9f5414464ffa8cf727ea2c0491e  mosquitto.initd
daaae48d49e49a9911f152dfc80f67f2371ad62647d3324b360eb83f816c0898469c135209ddb69e80a29b313bbe3b6ee11463955c97a44584921c13832a07be  disable-flaky-test.patch
b07f9bec2751ab32c43f53e74d8fca18dbf2d7ce7f8fab562dbcf75de19609ba6219d735ac504697404e0ed36613a14074e3a19e735297195877798f778d337a  openrc-conf.patch"
