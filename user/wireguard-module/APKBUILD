# Contributor: Mira Ressel <aranea@aixah.de>
# Maintainer: Laurent Bercot <ska-adelie@skarnet.org>
_kver="5.4.5-mc0"
pkgver=1.0.20200506
pkgrel=0
_pkgname="wireguard-module"
pkgname="$_pkgname-$_kver"
_pkgreal="wireguard-linux-compat"
pkgdesc="Kernel module for the WireGuard VPN, built for easy-kernel"
url="https://www.wireguard.com/"
arch="all"
options="!check !dbg !strip"  # No test suite.
license="GPL-2.0-only"
depends="easy-kernel-modules-$_kver"
makedepends="easy-kernel-src-$_kver"
provides="$_pkgname=$pkgver"
subpackages="wireguard-patch:_patch:noarch"
source="https://git.zx2c4.com/$_pkgreal/snapshot/$_pkgreal-$pkgver.tar.xz"
builddir="$srcdir/$_pkgreal-$pkgver"

build() {
	make -C src LDFLAGS="" KERNELDIR="/usr/src/linux-$_kver" module
}

package() {
	if [ -f $HOME/kernel_key.pem ]; then
		/usr/src/linux-$_kver/scripts/sign-file sha512 \
			$HOME/kernel_key.pem $HOME/kernel_key.pem \
			"$builddir"/src/wireguard.ko
	fi
	make -C src DEPMOD=true KERNELDIR="/usr/src/linux-$_kver" INSTALL_MOD_PATH="$pkgdir" module-install
}

_patch() {
	depends=""
	pkgdesc="Kernel patch for the WireGuard VPN"

	mkdir -p "$subpkgdir/usr/share/wireguard"
	"$builddir"/kernel-tree-scripts/create-patch.sh > "$subpkgdir/usr/share/wireguard/wireguard-$pkgver.patch"
}

sha512sums="39a27a515919933dbed71624be3f8f3f512073b522e1e16248c9eda749dd72a3db5a02d85d29855160eb182415f489a4c02c1659ef9589507c99dbfe74ea3074  wireguard-linux-compat-1.0.20200506.tar.xz"
