# Contributor: Kiyoshi Aman <adelie@aerdan.vulpine.house>
# Maintainer: Kiyoshi Aman <adelie@aerdan.vulpine.house>
pkgname=qtkeychain
pkgver=0.11.1
pkgrel=0
pkgdesc="Platform-independent Qt-based API for storing passwords securely"
url="https://github.com/frankosterfeld/qtkeychain"
arch="all"
options="!check"  # No test suite.
license="BSD-3-Clause"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qttools-dev"
subpackages="$pkgname-dev"
source="qtkeychain-$pkgver.tar.gz::https://github.com/frankosterfeld/qtkeychain/archive/v$pkgver.tar.gz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="45551003000b8ed76d44767c54d18dcbb956d5aeb63d8bf2f0fc06c61eab4d7d2e70b894e9c0729722dfcb5004f818f4f4c9a413444fc2aebe0271f104b500e6  qtkeychain-0.11.1.tar.gz"
