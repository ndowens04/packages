# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=purpose
pkgver=5.74.0
pkgrel=0
pkgdesc="KDE context menu framework"
url="https://www.kde.org/"
arch="all"
license="GPL-2.0+ AND LGPL-2.0+ AND LGPL-2.1+"
depends="kdeclarative"
makedepends="cmake extra-cmake-modules kconfig-dev kcoreaddons-dev ki18n-dev
	kio-dev kirigami2-dev knotifications-dev qt5-qtbase-dev
	qt5-qtdeclarative-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/purpose-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	# menutest requires X11
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E menutest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="dea52fd5d10cc0c34ecab742b691675076ddf1346e2b352cd5f74cd266576f85eb9d721bf68a29cea62f9879b826bf375b70580451c31bdd33509f57ae3cf23a  purpose-5.74.0.tar.xz"
