# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kauth
pkgver=5.74.0
pkgrel=0
pkgdesc="Framework for allowing software to gain temporary privileges"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1+"
depends=""
depends_dev="polkit-qt-1-dev qt5-qtbase-dev kcoreaddons-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qttools-dev doxygen"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kauth-$pkgver.tar.xz"

# secfixes:
#   5.54.0-r1:
#     - CVE-2019-7443

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E KAuthHelperTest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="277ff88cf447fdcf6c413997b4a3be871e7bddc0cd23cb379cb35f05c4b93cf68d7f0792eabc5d56ec7f043bf6de53ba9cec223750c4569a72e88c27f8d36036  kauth-5.74.0.tar.xz"
