# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=transmission
pkgver=2.94
pkgrel=0
pkgdesc="Lightweight GTK BitTorrent client"
url="https://transmissionbt.com/"
arch="all"
license="GPL-2.0+ AND MIT"
depends=""
makedepends="bsd-compat-headers curl-dev dbus-glib-dev gtk+3.0-dev intltool
	libevent-dev libnotify-dev openssl-dev qt5-qtbase-dev"
install="transmission.post-install transmission-daemon.pre-install transmission-daemon.post-upgrade"
pkgusers="transmission"
pkggroups="transmission"
source="https://github.com/transmission/$pkgname-releases/raw/master/$pkgname-$pkgver.tar.xz
	transmission-daemon.initd
	transmission-daemon.confd
	"
subpackages="$pkgname-qt $pkgname-gtk $pkgname-cli $pkgname-daemon $pkgname-doc $pkgname-lang"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--enable-utp \
		--with-inotify \
		--enable-cli
	make

	# build for Qt
	cd qt
	qmake qtr.pro
	make
	
}

check() {
	msg "Checking for Transmission Core and GTK"
	make check

	# check for Qt - this historically has no tests
	msg "Checking for Transmission Qt"
	make -C qt check
}

package() {
	make DESTDIR="$pkgdir" install

	# install for Qt
	make INSTALL_ROOT="$pkgdir/usr" -C qt install
}

qt() {
	pkgdesc="Lightweight BitTorrent client (Qt GUI interface)"
	depends="$pkgname=$pkgver-r$pkgrel"

	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/transmission-qt "$subpkgdir"/usr/bin
}

gtk() {
	pkgdesc="Lightweight BitTorrent client (Qt GUI interface)"
	depends="$pkgname=$pkgver-r$pkgrel"

	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/transmission-gtk "$subpkgdir"/usr/bin
}

daemon() {
	pkgdesc="Lightweight BitTorrent client (daemon and Web interface)"

	install -d "$subpkgdir"/usr/share \
		"$subpkgdir"/usr/bin
	install -d -o transmission -g transmission \
		"$subpkgdir"/var/lib/transmission \
		"$subpkgdir"/var/log/transmission
	mv "$pkgdir"/usr/bin/transmission-daemon \
	   "$subpkgdir"/usr/bin/
	mv "$pkgdir"/usr/share/transmission \
	   "$subpkgdir"/usr/share/
	install -D -m755 "$srcdir"/transmission-daemon.initd \
		"$subpkgdir"/etc/init.d/transmission-daemon
	install -D -m644 "$srcdir"/transmission-daemon.confd \
		"$subpkgdir"/etc/conf.d/transmission-daemon
}

cli() {
	pkgdesc="Lightweight BitTorrent client (CLI and remote)"

	install -d "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/transmission-cli \
	   "$pkgdir"/usr/bin/transmission-create \
	   "$pkgdir"/usr/bin/transmission-edit \
	   "$pkgdir"/usr/bin/transmission-show \
	   "$pkgdir"/usr/bin/transmission-remote \
	   "$subpkgdir"/usr/bin/
}

sha512sums="ee411743940f2897aa0bbc351ce79f11d860075d2e9e399d60301eae8cfc453e20426ef553fc62ee43019a07c052d512f5d7972cc4411fb57b1312c2c1558da7  transmission-2.94.tar.xz
d31275fba7eb322510f9667e66a186d626889a6e3143be2923aae87b9c35c5cf0c508639f1cb8c1b88b1e465bc082d80bb1101385ebde736a34d4eeeae0f6e15  transmission-daemon.initd
a3b9ac2b7bbe30e33060c8b6a693dc7072d3c6ac44f92ddd567969d8f57a0bfc1a561e781ae167703ccb4b2fd5b0e6d8f8a66c5ba14fe01d8d89a501d4501474  transmission-daemon.confd"
