# Contributor: Sergei Lukin <sergej.lukin@gmail.com>
# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Contributor: Jakub Skrzypnik <j.skrzypnik@openmailbox.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=ffmpeg
pkgver=4.2.2
pkgrel=1
pkgdesc="Record, convert, and stream audio and video"
url="https://ffmpeg.org/"
arch="all"
options="!check textrels"  # Test suite requires proper licensing headers on all files,
			   # which upstream does not provide.
license="GPL-2.0-only"
depends=""
makedepends="alsa-lib-dev bzip2-dev freetype-dev gnutls-dev imlib2-dev
	ladspa-dev lame-dev libcdio-dev libcdio-paranoia-dev libmodplug-dev
	libtheora-dev libva-dev libvdpau-dev libvorbis-dev libwebp-dev
	libxfixes-dev opus-dev perl-dev pulseaudio-dev sdl2-dev
	speex-dev v4l-utils-dev wavpack-dev x264-dev x265-dev xvidcore-dev
	xz-dev yasm zlib-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-libs"
source="https://ffmpeg.org/releases/ffmpeg-$pkgver.tar.xz
	0001-libavutil-clean-up-unused-FF_SYMVER-macro.patch
	CVE-2019-13312.patch
	CVE-2020-12284.patch
	"

# secfixes:
#   3.3.4-r0:
#     - CVE-2017-14054
#     - CVE-2017-14055
#     - CVE-2017-14056
#     - CVE-2017-14057
#     - CVE-2017-14058
#     - CVE-2017-14059
#     - CVE-2017-14169
#     - CVE-2017-14170
#     - CVE-2017-14171
#     - CVE-2017-14222
#     - CVE-2017-14223
#     - CVE-2017-14225
#   4.2.2-r1:
#     - CVE-2019-13312
#     - CVE-2020-12284

build() {
	_asm=""

	case "$CARCH" in
	ppc64) _asm="--cpu=G5" ;;
	pmmx) _asm="--disable-asm" ;;
	esac

	./configure \
		--prefix=/usr \
		--enable-avresample \
		--enable-avfilter \
		--enable-gnutls \
		--enable-gpl \
		--enable-libmp3lame \
		--enable-libmodplug \
		--enable-libvorbis \
		--disable-libvpx \
		--enable-libxvid \
		--enable-libx264 \
		--enable-libx265 \
		--enable-libtheora \
		--enable-libv4l2 \
		--enable-postproc \
		--enable-pic \
		--enable-pthreads \
		--enable-shared \
		--enable-libxcb \
		--disable-stripping \
		--disable-static \
		--enable-vaapi \
		--enable-vdpau \
		--enable-libopus \
		--enable-libcdio \
		--enable-ladspa \
		--enable-lzma \
		--enable-libspeex \
		--enable-libfreetype \
		--enable-libwavpack \
		--enable-libwebp \
		--enable-libpulse \
		--optflags="$CFLAGS" \
		$_asm
	make
	${CC:-gcc} -o tools/qt-faststart $CFLAGS tools/qt-faststart.c
}

package() {
	make DESTDIR="$pkgdir" install install-man
	install -D -m755 tools/qt-faststart "$pkgdir/usr/bin/qt-faststart"
}

libs() {
	pkgdesc="Libraries for ffmpeg"
	replaces="ffmpeg"
	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/lib "$subpkgdir"/usr/
}

sha512sums="381cd6732fa699eb89000621cf34256920596ed1f9de3c2194dbad35fdf2165269eb7d3a147a0eb75dc18fbb6d601382b5801750e09fc63547766842f84208e3  ffmpeg-4.2.2.tar.xz
1047a23eda51b576ac200d5106a1cd318d1d5291643b3a69e025c0a7b6f3dbc9f6eb0e1e6faa231b7e38c8dd4e49a54f7431f87a93664da35825cc2e9e8aedf4  0001-libavutil-clean-up-unused-FF_SYMVER-macro.patch
0c53680ae480b8f848893d4e5c40ea522bd25a72860e0955e039ec838ee09159ab2bfa0eafc71113009082c7f53981ba70116dcef17053cd3cc3ea59e4da5a5c  CVE-2019-13312.patch
910f8da9ed8e0998c311cc451f1725a20c4cc3e9f0c2d1981a1ceea8da5f434519044b4997e71d87424e19fcc45cb203238e49ad178e313696667e6c9bf311c9  CVE-2020-12284.patch"
