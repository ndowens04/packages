# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kcmutils
pkgver=5.74.0
pkgrel=0
pkgdesc="Framework for writing System Settings modules"
url="https://api.kde.org/frameworks/kcmutils/html/index.html"
arch="all"
options="!check"  # Test suite requires running X11 session.
license="LGPL-2.1-only AND LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev qt5-qtdeclarative-dev kitemviews-dev kservice-dev
	kconfigwidgets-dev kiconthemes-dev kdeclarative-dev kxmlgui-dev"
makedepends="$depends_dev cmake extra-cmake-modules python3 doxygen graphviz
	qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kcmutils-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="105664749e73eeb0b911479d3befe813eef290d7dafa4ec51c134a79c58826361f90e18acf6d8f5d5b7e8d5d6a5956a73bc31e2c6f3de9ad45fec80341963c7a  kcmutils-5.74.0.tar.xz"
