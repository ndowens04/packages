# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=gc
pkgver=8.0.4
pkgrel=0
pkgdesc="A garbage collector for C and C++"
url="https://hboehm.info/gc/"
arch="all"
license="MIT"
depends=""
makedepends="libatomic_ops-dev linux-headers"
subpackages="$pkgname-dev $pkgname-doc libgc++:libgccpp"
source="https://github.com/ivmai/bdwgc/releases/download/v$pkgver/$pkgname-$pkgver.tar.gz"

build() {
	if [ "$CLIBC" = "musl" ]; then
		export CFLAGS="$CFLAGS -D_GNU_SOURCE -DNO_GETCONTEXT -DUSE_MMAP -DHAVE_DL_ITERATE_PHDR"
	fi
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--datadir=/usr/share/doc/gc \
		--enable-cplusplus
	make
}

check() {
	if [ "$CARCH" = "ppc" ]; then
		make check || true
	else
		make check
	fi
}

package() {
	make DESTDIR="$pkgdir" install
}

libgccpp() {
	install -d "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libgccpp.* "$subpkgdir"/usr/lib/
}

sha512sums="57ccca15c6e50048d306a30de06c1a844f36103a84c2d1c17cbccbbc0001e17915488baec79737449982da99ce5d14ce527176afae9ae153cbbb5a19d986366e  gc-8.0.4.tar.gz"
